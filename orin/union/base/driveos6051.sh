#!/bin/bash

set -e
set -x

function install_cuda() {
    sudo dpkg -i ./cuda-repo-ubuntu2004-11-4-local_11.4.18-470.141.03-1_amd64.deb
    sudo cp /var/cuda-repo-ubuntu2004-11-4-local/cuda-51997F51-keyring.gpg /usr/share/keyrings/
    sudo dpkg -i ./cuda-repo-cross-aarch64-ubuntu2004-11-4-local_11.4.18-1_all.deb
    sudo cp /var/cuda-repo-cross-aarch64-ubuntu2004-11-4-local/cuda-32437944-keyring.gpg /usr/share/keyrings/
    sudo apt update
    sudo apt install cuda-toolkit-11-4 cuda-cross-aarch64-11-4 -y

    # 1.Install the below CUDA 11.4 Debian packages for Linux available under the same folder.
    # sudo dpkg -i ./cuda-repo-ubuntu2004-11-4-local_[CUDA-VERSION]-[DRIVER-VERSION]-1_amd64.deb
    # sudo dpkg -i ./cuda-repo-cross-aarch64-ubuntu2004-11-4-local_[CUDA-VERSION]-1_all.deb
    # sudo apt-key add /var/cuda-repo-ubuntu2004-11-4-local/51997F51.pub
    # sudo apt-key add /var/cuda-repo-cross-aarch64-ubuntu2004-11-4-local/32437944.pub
    # sudo apt update
    # sudo apt -y install cuda-toolkit-11-4
    # sudo apt -y install cuda-cross-aarch64-11-4

    # 2.In case of any issue with the installation, remove old packages and reinstall.
    # sudo rm /var/lib/apt/lists/_var_cuda*
    # sudo apt --fix-broken install -y
    # sudo apt-get autoremove -y
    # sudo apt-get remove --purge -y "cuda*"
    # sudo apt-get remove --purge -y "*cublas*"
}

function install_cudnn() {
    sudo apt install ./cudnn-local-repo-ubuntu2004-8.4.1.153_1.0-1_amd64.deb
    sudo cp /var/cudnn-local-repo-ubuntu2004-8.4.1.153/cudnn-local-44B3C3AF-keyring.gpg /usr/share/keyrings/
    sudo apt install ./cudnn-local-repo-cross-aarch64-ubuntu2004-8.4.1.153_1.0-1_all.deb
    sudo cp /var/cudnn-local-repo-cross-aarch64-ubuntu2004-8.4.1.153/cudnn-local-8DD81901-keyring.gpg /usr/share/keyrings/
    sudo apt update
    sudo apt install libcudnn8 libcudnn8-dev libcudnn8-samples libcudnn8-cross-aarch64 -y

    # 3.Install the cuDNN Debian packages for Linux:
    # sudo apt install ./cudnn-local-repo-ubuntu2004-[CUDNN-VERSION].deb
    # sudo apt-key add /var/cudnn-local-repo-ubuntu2004-[CUDNN-VERSION]/44B3C3AF.pub
    # sudo apt update
    # sudo apt install libcudnn8
    # sudo apt install libcudnn8-dev
    # sudo apt install libcudnn8-samples

    # 4.Install the cuDNN Debian packages for cross-compiling on Linux:
    # sudo apt install ./cudnn-local-repo-cross-aarch64-ubuntu2004-[CUDNN-VERSION].deb
    # sudo apt-key add /var/cudnn-local-repo-cross-aarch64-ubuntu2004-[CUDNN-VERSION]/8DD81901.pub
    # sudo apt update
    # sudo apt install libcudnn8-cross-aarch64
    # sudo apt install libcudnn8-dev-cross-aarch64
}

function install_tensorrt() {
    sudo dpkg -i ./nv-tensorrt-repo-ubuntu2004-cuda11.4-trt8.4.12.5-x86-host-ga-20220921_1-1_amd64.deb
    sudo apt-key add /var/nv-tensorrt-repo-ubuntu2004-cuda11.4-trt8.4.12.5-x86-host-ga-20220921/096ac445.pub
    sudo dpkg -i ./nv-tensorrt-repo-ubuntu2004-cuda11.4-trt8.4.12.5-d6l-cross-ga-20220921_1-1_amd64.deb
    sudo apt-key add /var/nv-tensorrt-repo-ubuntu2004-cuda11.4-trt8.4.12.5-d6l-cross-ga-20220921/096ac445.pub
    sudo apt update
    sudo apt install tensorrt tensorrt-dev tensorrt-libs tensorrt-cross-aarch64 tensorrt-cross-aarch64-dev tensorrt-cross-aarch64-libs -y

    # 5.Install TensorRT Debian packages for Linux:
    # sudo dpkg -i nv-tensorrt-repo-ubuntu2004-cuda11.4-trt[RELEASE]-d6l-host-{ea/ga}-[BUILD-DATE]_1-1_amd64.deb
    # sudo dpkg -i nv-tensorrt-repo-ubuntu2004-cuda11.4-trt[RELEASE]-x86-host-[ea | ga]-[BUILD-DATE]_1-1_amd64.deb
    # sudo apt-key add /var/nv-tensorrt-repo-ubuntu2004-cuda11.4-trt[RELEASE]-d6l-cross-{ea/ga}-[BUILD-DATE]/4790c9e1.pub
    # sudo apt update
    # sudo apt install tensorrt
}

function delete_deb() {
    set +e
    rm -rf cuda-repo-ubuntu2004-11-4-local_11.4.18-470.141.03-1_amd64.deb \
        cuda-repo-cross-aarch64-ubuntu2004-11-4-local_11.4.18-1_all.deb \
        cudnn-local-repo-ubuntu2004-8.4.1.153_1.0-1_amd64.deb \
        cudnn-local-repo-cross-aarch64-ubuntu2004-8.4.1.153_1.0-1_all.deb \
        nv-tensorrt-repo-ubuntu2004-cuda11.4-trt8.4.12.5-x86-host-ga-20220921_1-1_amd64.deb \
        nv-tensorrt-repo-ubuntu2004-cuda11.4-trt8.4.12.5-d6l-cross-ga-20220921_1-1_amd64.deb

    cd /var
    rm -rf cuda-repo-ubuntu2004-11-4-local \
        cuda-repo-cross-aarch64-ubuntu2004-11-4-local \
        cudnn-local-repo-ubuntu2004-8.4.1.153 \
        cudnn-local-repo-cross-aarch64-ubuntu2004-8.4.1.153 \
        nv-tensorrt-repo-ubuntu2004-cuda11.4-trt8.4.12.5-x86-host-ga-20220921 \
        nv-tensorrt-repo-ubuntu2004-cuda11.4-trt8.4.12.5-d6l-cross-ga-20220921

    cd /etc/apt/sources.list.d
    rm -rf ./*.list
    set -e
}

function install() {
    install_cuda
    install_cudnn
    install_tensorrt
    delete_deb
}

function main() {
    if [ $# = 0 ]; then
        install
    elif [ $# = 1 ]; then
        if [ "$1" = "i" ] || [ "$1" = "install" ]; then
            install
        else
            echo "Error!"
        fi
    else
        echo "Error!"
    fi
}

main "$@"

exit 0
