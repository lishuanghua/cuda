#!/bin/bash

set -e
set -x

# echo -e "\033[32m Green OK! \033[0m"
# echo -e "\033[33m Yellow Warnning! \033[0m"
# echo -e "\033[31m Red Error! \033[0m"

BUILD_REPOS=(cyber common_neolix cyberverse EngineSDK calibration canbus control dreamview
    gateway drivers guardian_cyber planning prediction openAPI ranger perception tools)

function clone_repos() {
    local BRANCH="orin_dev"

    local CLONE_REPOS=(system/cyber system/common_neolix map/cyberverse perception/calibration pnc/canbus
        pnc/control data/dreamview data/gateway system/drivers system/guardian_cyber pnc/planning
        pnc/prediction data/openAPI localization/ranger perception/EngineSDK perception/perception system/tools)

    local USERNAME=$1
    local PASSWORD=$2

    if [ -z "${USERNAME}" ] || [ -z "${PASSWORD}" ]; then
        read -p "Pleasen enter your gitlab name:" USERNAME
        read -sp "Pleasen enter your gitlab password:" PASSWORD
    fi

    for clone_repo in "${CLONE_REPOS[@]}"; do
        local repo_name=$(echo ${clone_repo} | awk -F '/' '{print $2}')
        if [ ! -d /home/caros/"${repo_name}" ]; then
            /usr/bin/expect <<-EOF
set timeout 300
spawn git clone -b ${BRANCH} http://git.neodrive.neolix.net/neolix/${clone_repo}.git /home/caros/${repo_name}
expect {
 "Username for 'http://git.neodrive.neolix.net':" { send "${USERNAME}\r"; exp_continue }
 "Password for 'http://${USERNAME}@git.neodrive.neolix.net':"  { send "${PASSWORD}\r" }
}
expect eof
EOF
            echo -e "\033[32m Clone repo ${repo_name} successfully! \033[0m"
        else
            echo -e "\033[31m Repo ${repo_name} is already exists! \033[0m"
        fi
    done

    echo -e "\033[32m All repos are cloned successfully! \033[0m"
}

function pull_repos() {
    for repo in "${BUILD_REPOS[@]}"; do
        if [ -d /home/caros/"${repo}" ]; then
            cd /home/caros/"${repo}"
            git pull
            cd -
        else
            echo -e "\033[31m Directory ${repo} is not exists! \033[0m"
            exit 1
        fi
        echo -e "\033[32m git pull ${repo} successfully! \033[0m"
    done
}

function change_branches() {
    for repo in "${BUILD_REPOS[@]}"; do
        if [ -d /home/caros/"${repo}" ]; then
            cd /home/caros/"${repo}"
            if git branch -a | grep "remotes/origin/orin_dev_drive_orin_docker_lishuanghua"; then
                git checkout -b orin_dev_drive_orin_docker_lishuanghua origin/orin_dev_drive_orin_docker_lishuanghua
                git config --global user.email "you@example.com"
                git config --global user.name "Your Name"
                git rebase orin_dev
            fi
            cd -
        else
            echo -e "\033[31m Directory ${repo} is not exists! \033[0m"
            exit 1
        fi
        echo -e "\033[32m change ${repo} to branch orin_dev_drive_orin_docker_lishuanghua successfully! \033[0m"
    done
}

function build_repos() {
    for repo in "${BUILD_REPOS[@]}"; do
        if [ -d /home/caros/"${repo}" ]; then
            cd /home/caros/"${repo}"
            bash cross_build.sh
            cd -
        else
            echo -e "\033[31m Directory ${repo} is not exists! \033[0m"
        fi
        echo -e "\033[32m build ${repo} successfully! \033[0m"
    done
    echo -e "\033[32m All repos are built successfully! \033[0m"
}

function clean_repos() {
    for repo in "${BUILD_REPOS[@]}"; do
        if [ -d /home/caros/"${repo}" ]; then
            cd /home/caros/"${repo}"
            bash cross_build.sh clean
            cd -
        else
            echo -e "\033[31m Directory ${repo} is not exists! \033[0m"
        fi
        echo -e "\033[32m clean ${repo} successfully! \033[0m"
    done
    echo -e "\033[32m All repositories clean successfully! \033[0m"
}

function print_usage() {
    echo -e "\033[32m To be done! \033[0m"
}

function main() {
    local cmd=$1
    case ${cmd} in
    clone)
        clone_repos $2 $3
        change_branches
        ;;
    pull)
        pull_repos $2 $3
        ;;
    build)
        build_repos
        ;;
    clean)
        clean_repos
        ;;
    *)
        print_usage
        ;;
    esac
}

main $@
