#!/bin/bash

set -e
set -x

# echo -e "\033[32m Green OK! \033[0m"
# echo -e "\033[33m Yellow Warnning! \033[0m"
# echo -e "\033[31m Red Error! \033[0m"

function clone_gears() {
    if [ ! -d "/home/caros/gears" ]; then

        local USERNAME=$1
        local PASSWORD=$2

        if [ -z "${USERNAME}" ] || [ -z "${PASSWORD}" ]; then
            read -p "Pleasen enter your gitlab name:" USERNAME
            read -sp "Pleasen enter your gitlab password:" PASSWORD
        fi

        /usr/bin/expect <<-EOF

set timeout 300

spawn git clone -b gears_gcc9_orin_lishuanghua http://git.neodrive.neolix.net/neolix/system/gears_gcc8.git /home/caros/gears
expect {
 "Username for 'http://git.neodrive.neolix.net':" { send "${USERNAME}\r"; exp_continue }
 "Password for 'http://${USERNAME}@git.neodrive.neolix.net':"  { send "${PASSWORD}\r" }
}

expect eof
EOF

        echo -e "\033[32m Green OK! Clone gears successfully! \033[0m"
    else
        echo -e "\033[33m Yellow Warnning! Gears already exists, no need to copy, skip! \033[0m"
    fi
}

function build_gears() {
    if [ -d "/home/caros/gears" ]; then
        cd /home/caros/gears/src
        bash install_gears.sh
        cd -
        echo -e "\033[32m Green OK! Gears build successfully! \033[0m"
    else
        echo -e "\033[31m Red Error! Gears not exists, please check it! \033[0m"
        exit 1
    fi
}

function main() {
    clone_gears $1 $2
    build_gears
}

main $@
